# 无人机视觉自主导航

#### Description
Graduation project；

Mentor：布树辉；

Subject：Visual SLAM based Autonomous Navigation for UAV

Related technology ：light glue;slam;NetVLAD


#### Periodic progress in learning

1.  read the thesis
2.  master Machine learning
    [Wu Enda's video](https://www.bilibili.com/video/BV1pm4y1T7wx/?spm_id_from=333.999.0.0&vd_source=68c3a357e97fb6272ec51ffeff167695)
    
    [Bu Shuhui's video](https://www.bilibili.com/video/BV1SK411f7G7?p=1&vd_source=68c3a357e97fb6272ec51ffeff167695)
3.  learn pythorch 
4.  build a simple AI on myself



#### Thesis 

1.  NetVLAD: CNNarchitecture for weakly supervised place recognition
2.  Vision-based GNSS-Free Localization for UAVs in the Wild[ The open source project](https://github.com/TIERS/wildnav)
3.  LightGlue: Local Feature Matching at Light Speed
